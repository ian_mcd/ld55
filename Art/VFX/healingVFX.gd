extends Node3D
class_name healingVFX

@onready var heal = $GPUParticles3D

func _ready():
	heal.emitting = true
	await get_tree().create_timer(7).timeout
	queue_free()
