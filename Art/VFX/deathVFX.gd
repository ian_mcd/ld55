extends Node3D
class_name deathVFX


@onready var death = $GPUParticles3D

func _ready():
	death.emitting = true
	await get_tree().create_timer(1).timeout
	queue_free()
