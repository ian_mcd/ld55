extends Node3D
class_name GodAttackVFX

@onready var godAttack1 = $particleBlast
@onready var godAttack2 = $RingExpand

func _ready():
	godAttack1.emitting = true
	godAttack2.emitting = true
	await get_tree().create_timer(5).timeout
	queue_free()
