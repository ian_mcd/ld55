extends Node3D
class_name hit_VFX

@onready var hit = $hitVFX

func _ready():
	hit.emitting = true
	await get_tree().create_timer(1).timeout
	queue_free()
