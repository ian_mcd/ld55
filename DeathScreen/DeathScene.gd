extends Control


@onready var restart_button = $CenterContainer/VBoxContainer2/PlayButton as Button
@onready var quit_button = $CenterContainer/VBoxContainer2/QuitButton as Button
@onready var restart_level = preload("res://Game.tscn")
@onready var title_level = preload("res://TitleScreen/TitleScene.tscn")

# Called when the node enters the scene tree for the first time.
func _ready():
	restart_button.button_up.connect(on_restart)
	quit_button.button_up.connect(on_quit)


# Called every frame. 'delta' is the elapsed time since the previous frame.
func on_restart():
	get_tree().change_scene_to_packed(restart_level)
	
func on_quit():
	get_tree().change_scene_to_packed(title_level)
