class_name moneyCounterUI extends Control

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	if Player.player != null:
		changeDisplayMoney(Player.player.money)
func changeDisplayMoney(amount):
	self.get_child(0).bbcode_text = "$ " + str(amount)
