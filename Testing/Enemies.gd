extends Node3D


# Called when the node enters the scene tree for the first time.
func _ready():
	for c in get_children():
		if c is Summon:
			var summon: Summon = c as Summon
			summon.target.allegiance = Target.Allegiance.ENEMY
