extends Control


@onready var start_button = $MarginContainer/PanelContainer/MarginContainer/VBoxContainer/PlayButton as Button
@onready var quit_button = $MarginContainer/PanelContainer/MarginContainer/VBoxContainer/QuitButton as Button
@onready var start_level = preload("res://Game.tscn")

# Called when the node enters the scene tree for the first time.
func _ready():
	start_button.button_up.connect(on_start)
	quit_button.button_up.connect(on_quit)


# Called every frame. 'delta' is the elapsed time since the previous frame.
func on_start():
	get_tree().change_scene_to_packed(start_level)
	
func on_quit():
	get_tree().quit()
