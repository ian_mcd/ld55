extends CharacterBody3D
class_name Player 

static var player: Player
var itemCostHolder = 0
var itemNameHolder = ""
var money = 30
var podiumInstance: ShopPodium = ShopPodium.new()
var shopWindow = ShopUI.new()
var moneyUI = moneyCounterUI.new()
# How fast the player moves in meters per second.
@export var speed = 8
# The downward acceleration when in the air, in meters per second squared.
@export var fall_acceleration = 75
static var buyingState = false 

signal on_death

var target: Target

var target_velocity = Vector3.ZERO
var podiumOverlapp = false
var podium = null
 
var curr_shop_data: SummonData

var anim_player: AnimationPlayer

func _init():
	player = self

func _enter_tree():
	update_model()

func _ready():
	shopWindow = $"../PlaceholderPopup"
	moneyUI =$"../MoneyUI" 
	
	target.health.on_zero.connect(_on_death)

	for c in $Pivot/summoner.get_children():
		if c is AnimationPlayer:
			anim_player = c
	
	if anim_player != null:
		anim_player.play("summoner_idle")

func _on_death():
	on_death.emit()
	# You have died.
	GameManager.instance.on_loose()
	queue_free()

func _physics_process(_delta):
	var direction = Vector3.ZERO

	if Input.is_action_pressed("move_right"):
		direction.x += 1
	if Input.is_action_pressed("move_left"):
		direction.x -= 1
	if Input.is_action_pressed("move_down"):
		direction.z += 1
	if Input.is_action_pressed("move_up"):
		direction.z -= 1
	
	if direction != Vector3.ZERO:
		direction = direction.normalized()
		$Pivot.basis = Basis.looking_at(direction)

	# Ground Velocity
	
	target_velocity.x = direction.x * speed
	target_velocity.z = direction.z * speed
	

	# Moving the Character
	velocity = target_velocity
	move_and_slide()
	if anim_player != null:
		if velocity.length() == 0:
			anim_player.play("summoner_idle")
		else:
			anim_player.play("summoner_walk")
	
	if (Input.is_action_just_released("left_mb") && podiumOverlapp && buyingState==false):
		shopWindow.changePopupText(itemNameHolder, itemCostHolder)
		print_debug("Overlapping podium")
		buyingState = true
		$"../PlaceholderPopup".show()
		
	elif(Input.is_action_just_released("left_mb") && !podiumOverlapp):
		pass

func update_model():
	for child in get_children():
		if child is Target:
			target = child
			target.update_model()
			return

func on_player_enter_podium_area(area):
	podiumInstance = area.get_parent_node_3d() as ShopPodium
	podiumOverlapp = true
	itemCostHolder = podiumInstance.getItemCost()
	itemNameHolder = podiumInstance.getItemAttatched()
	curr_shop_data = podiumInstance.shop_item_podium.summon_data


func on_player_exit_podium_area(_area):
	
	#exit the shopping state
	podiumOverlapp = false
	buyingState = false
	$"../PlaceholderPopup".hide()
	podiumInstance = null
	curr_shop_data = null

func _on_do_not_buy_podium():
	buyingState = false
	pass # Replace with function body.

@export var packed_card_scene: PackedScene

func make_card(summon: SummonData) -> Card:
	var card = packed_card_scene.instantiate() as Card
	var name_string: String = "PLAYER:" + summon.name
	print(name_string)
	card.name = name_string
	card.summon_data = summon
	return card


func _on_buy_podium():
	if curr_shop_data == null:
		print("SHIT")
	if money - itemCostHolder >= 0:
		buyingState = false
		print_debug("Player bought " + itemNameHolder + " for " + str(itemCostHolder))
		changePlayerMoney(-itemCostHolder)
		
		var new_card: Card = make_card(curr_shop_data)
		Summoner.instance.deck.add_card(new_card)

func changePlayerMoney(amount) -> int:
	money += amount
	return money
	
func getPlayerMoney() ->int:
	return money
