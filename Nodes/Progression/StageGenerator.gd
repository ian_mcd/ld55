extends Resource

class_name StageGenerator

@export var base_reward: int = 5
@export var base_deck_points: int = 1
@export var available_summons: Array[SummonData]
@export var maps: Array[PackedScene]

var stages: Array[StageData]
	
func make_stages(amount_of_stages: int) -> Array[StageData]:
	for stage_number in range(1, amount_of_stages + 1):
		var new_stage := StageData.new()
		new_stage.objective_type = choose_objective_type(stage_number, amount_of_stages)
		new_stage.resource_name = generate_stage_name(stage_number)
		new_stage.map = choose_stage_map()
		new_stage.enemy_summons = generate_enemy_deck(stage_number, new_stage.objective_type)
		new_stage.reward = generate_stage_reward(stage_number)
		stages.append(new_stage)
	return stages
	
func choose_objective_type(stage_number: int, amount_of_stages: int) -> StageData.ObjectiveType:
	# TODO:  be smart
	return StageData.ObjectiveType.ENEMY_SUMMONER
	#if stage_number == amount_of_stages:
		#return StageData.ObjectiveType.BOSS_SUMMON
	#else: 
		#return [StageData.ObjectiveType.ENEMY_SUMMONER, StageData.ObjectiveType.STATIC_SUMMONS][randi() % 2]

func choose_stage_map() -> PackedScene:
	if maps.is_empty():
		return null
	return maps[randi() % maps.size()]

func generate_stage_name(stage_number: int) -> String:
	return ("Stage %s" % stage_number as String)

func generate_enemy_deck(stage_number: int, objective_type: StageData.ObjectiveType) -> Array[SummonData]:
	match objective_type:
		StageData.ObjectiveType.BOSS_SUMMON:
			return generate_boss_deck(stage_number)
		_:
			return generate_basic_enemy_deck(stage_number)
	
func generate_basic_enemy_deck(stage_number: int) -> Array[SummonData]:
	var deck: Array[SummonData] = []
	var total_deck_points = stage_number * base_deck_points
	
	while total_deck_points > 0:
		var purchaseable_summons = available_summons.filter(
				func(summon: SummonData): return summon.point_cost <= total_deck_points
			)
		purchaseable_summons.shuffle()
		var purchased_summon: SummonData = purchaseable_summons.pop_front()
		total_deck_points -= purchased_summon.point_cost
		deck.append(purchased_summon)
	
	return deck

func generate_boss_deck(stage_number: int):
	var deck: Array[SummonData] = []

	return deck
	
func generate_stage_reward(stage_number: int) -> int:
	return base_reward * stage_number
