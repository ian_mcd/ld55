extends Node

class_name StageManager

@export var number_of_stages: int = 10

@export var generator: StageGenerator
@export var base_stage_scene: PackedScene

#signal all_stages_completed
var stages: Array[StageData]

var _current_stage_idx: int = -1

var current_stage: Stage

var current_stage_data: StageData:
	get: return stages[_current_stage_idx]

static var instance: StageManager

signal on_stage_load(stage_data: StageData, stage: Stage)

func _ready():
	instance = self
	stages = generator.make_stages(number_of_stages)
	load_next_stage()

func load_stage(stage_data: StageData) -> Stage:
	if base_stage_scene == null: 
		print("Null base stage")
		return
	
	if not base_stage_scene.can_instantiate():
		print("Cannot Instantiate Base Stage Scene")
		return
	
	var new_stage = base_stage_scene.instantiate() as Stage
	new_stage.populate(stage_data)
	current_stage = new_stage
	add_child(new_stage)
	on_stage_load.emit(stage_data, new_stage)
	return new_stage

func unload_current_stage():
	remove_child(current_stage)
	current_stage.queue_free()
	current_stage = null

func load_next_stage() -> Stage:
	print("Loading Stage")
	_current_stage_idx += 1
	# TODO: handle end of stages
	return load_stage(current_stage_data)

