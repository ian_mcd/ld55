extends Node3D

class_name Stage

var stage_data: StageData

var enemy_summoner_scene: PackedScene = preload("res://Nodes/EnemySummoner/enemy_summoner.tscn")

var objective: Objective

var reward: int = 0

func populate(new_stage_data: StageData):
	stage_data = new_stage_data
	print("Populating %s" % stage_data.resource_name)
	var spawns: Dictionary = create_map(stage_data.map)
	var enemy_spawns: Array[Node3D] = spawns["EnemySpawns"] as Array[Node3D]
	var player_spawn: Node3D = spawns["PlayerSpawn"] as Node3D

	var deck: Deck = create_enemy_deck(stage_data.enemy_summons)
	var targets: Array[Target] = create_enemy(new_stage_data.objective_type, deck, enemy_spawns)
	create_objective(new_stage_data, targets)
	create_starting_enemies(stage_data)
	Player.player.position = player_spawn.position

func _reward_player():
	Player.player.money += reward

func create_objective(_new_stage_data: StageData, targets: Array[Target]):
	objective = Objective.new()
	# TODO: Generate objective
	objective.targets = targets
	objective.objective_complete.connect(_reward_player)
	add_child(objective)

func create_enemy(objective_type: StageData.ObjectiveType, deck: Deck, enemy_spawns: Array[Node3D]) -> Array[Target]:
	if objective_type == StageData.ObjectiveType.ENEMY_SUMMONER:
		var enemy_summoner: EnemySummoner = enemy_summoner_scene.instantiate() as EnemySummoner
		deck.name = "Deck"
		enemy_summoner.add_child(deck)
		add_child(enemy_summoner)
		enemy_summoner.position = enemy_spawns[0].position
		enemy_summoner.position = enemy_spawns[0].position
		print(enemy_summoner.position)
		return [enemy_summoner.target]
	else:
		add_child(deck)
		# TODO: spawn deck at spawnpoints.
	return []

func create_enemy_deck(summons: Array[SummonData]) -> Deck:
	var deck = Deck.new()
	deck.name = "EnemyDeck"
	
	for summon in summons:
		reward += summon.powerLevel
		var card = make_card(summon)
		deck.add_child(card)
	
	return deck

func make_card(summon: SummonData) -> Card:
	var card = Card.new()
	var name_string: String = "%s %s Card" % [stage_data.resource_name, summon.name]
	print(name_string)
	card.name = name_string
	card.summon_data = summon
	return card

func create_map(map_scene: PackedScene) -> Dictionary:
	if map_scene == null: 
		print("Null Map Scene")
		return {}
		
	if not map_scene.can_instantiate():
		print("Map Scene Cannot Instantiate!")
		return {}

	var map = map_scene.instantiate()
	add_child(map)

	var enemy_spawns: Array[Node3D] = []
	var player_spawn: Node3D = null
	
	var nodes_to_process: Array[Node] = [map]

	while len(nodes_to_process) > 0:
		var n: Node = nodes_to_process.pop_back()
		nodes_to_process.append_array(n.get_children())

		if n is Node3D:
			if n.is_in_group("EnemySpawn"):
				enemy_spawns.append(n as Node3D)
			elif n.is_in_group("PlayerSpawn"):
				player_spawn = n as Node3D

	return {"EnemySpawns": enemy_spawns, "PlayerSpawn": player_spawn}
	
func create_starting_enemies(new_stage_data: StageData):
	match new_stage_data.objective_type:
		StageData.ObjectiveType.ENEMY_SUMMONER:
			print("ENEMY_SUMMONER")
		StageData.ObjectiveType.STATIC_SUMMONS:
			print("STATIC_SUMMONS")
		StageData.ObjectiveType.BOSS_SUMMON:
			print("BOSS_SUMMON")
		_:
			print("What the heck happened")
