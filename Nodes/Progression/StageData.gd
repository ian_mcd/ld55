extends Resource

class_name StageData

enum ObjectiveType {
	ENEMY_SUMMONER,
	STATIC_SUMMONS,
	BOSS_SUMMON
}

@export var map: PackedScene = null
@export var objective_type: ObjectiveType = ObjectiveType.ENEMY_SUMMONER
@export var enemy_summons: Array[SummonData] = []
# @export var modifiers: Array[Modifiers]
@export var reward: int = 0
