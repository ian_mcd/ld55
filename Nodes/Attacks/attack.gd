class_name Attack extends ModelNode

var targeting_pattern: TargetingPattern

@export
var animation_name: String

var attack_effect: AttackEffect

var attack_range: float:
	get: return get_range()

var cooldown: float:
	get: return get_cooldown() * 1000

var base_damage: int:
	get: return get_base_damage()

var alligence: Target.Allegiance:
	get: return get_allegence()
	
var summon: Summon

func update_model():
	summon = $"../.."
	
	var num_items: int = 2
	for child in get_children():
		if child is TargetingPattern:
			targeting_pattern = child as TargetingPattern
			num_items -= 1
		if child is AttackEffect:
			attack_effect = child as AttackEffect
			num_items -= 1
		# Early return if we have collected everything.
		if num_items == 0:
			return

func _enter_tree():
	update_model()

func attack(_target: Target) -> AttackApply:
	return null

func do_attack(target: Target) -> AttackApply:
	if attack_effect != null:
		attack_effect.apply(target)
	return attack(target)

func get_range() -> float:
	return 0

func get_cooldown() -> float:
	return 0

func get_base_damage() -> int:
	return -1

func get_allegence() -> Target.Allegiance:
	return Target.Allegiance.NEUTRAL

static func load_dict(_data: Dictionary) -> Attack:
	return null
