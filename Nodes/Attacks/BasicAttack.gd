class_name BasicAttack extends Attack

const DEFAULT_RANGE = 1.1
const DEFAULT_COOLDOWN = 1
const DEFAULT_DAMAGE = 3

@export
var _range: float = DEFAULT_RANGE
func get_range() -> float:
	return _range

@export
var _cooldown: float = DEFAULT_COOLDOWN
func get_cooldown() -> float:
	return _cooldown

@export
var _base_damage: int = DEFAULT_DAMAGE
func get_base_damage() -> int:
	return _base_damage

@export
var self_target: Target


func update_model():
	super()
	# Find Target
	for child in self.summon.get_children():
		if child is Target:
			self_target = child
			break

func get_allegence() -> Target.Allegiance:
	return Target.get_oposite_allegiance(self_target.allegiance)

var _attack_application: AttackApply

func _init():
	_attack_application = AttackApply.new()
	_attack_application.damage = _base_damage

func attack(target: Target) -> AttackApply:
	target.apply(_attack_application)
	return _attack_application
