class_name BasicHeal extends Attack

const DEFAULT_RANGE = 1.1

const DEFAULT_COOLDOWN = 1
const DEFAULT_DAMAGE = 3

@export
var _range: float = DEFAULT_RANGE
func get_range() -> float:
	return _range

@export
var _cooldown: float = DEFAULT_COOLDOWN
func get_cooldown() -> float:
	return _cooldown

@export
var _base_heal: int = DEFAULT_DAMAGE
func get_base_damage() -> int:
	return _base_heal

var self_target: Target

var _attack_application: AttackApply

func update_model():
	super()
	# Find Target
	for child in self.summon.get_children():
		if child is Target:
			self_target = child
			break

func _ready():
	_attack_application = AttackApply.new()
	_attack_application.heal = _base_heal

func attack(target: Target) -> AttackApply:
	target.apply(_attack_application)
	return _attack_application

func get_allegence() -> Target.Allegiance:
	return self_target.allegiance
