class_name RangedAttack extends Attack

const DEFAULT_RANGE = .5
const DEFAULT_COOLDOWN = 1
const DEFAULT_DAMAGE = 3

@export var projectile_spawn: Node3D

@export var projectile_delay: float = 0

var attack_process: Array[Dictionary] = []

@export
var projectile_scene: PackedScene

@export
var _range: float = DEFAULT_RANGE
func get_range() -> float:
	return _range

@export
var _cooldown: float = DEFAULT_COOLDOWN
func get_cooldown() -> float:
	return _cooldown


@export
var _base_damage: int = DEFAULT_DAMAGE
func get_base_damage() -> int:
	return _base_damage

@export
var self_target: Target

func get_allegence() -> Target.Allegiance:
	return Target.get_oposite_allegiance(self_target.allegiance)

var _attack_application: AttackApply

func _init():
	_attack_application = AttackApply.new()
	_attack_application.damage = _base_damage

func _ready():
	if projectile_spawn == null:
		projectile_spawn = self_target

func _process(_delta):
	
	while len(attack_process) != 0 and attack_process[0]["time"] > Time.get_ticks_msec():
		_create_and_fire(attack_process[0]["target"])
		attack_process.pop_front()

func _create_and_fire(target: Target):
	var projectile: Projectile = projectile_scene.instantiate() as Projectile
	projectile.create(target, _attack_application)
	projectile.position = projectile_spawn.global_position
	StageManager.instance.current_stage.add_child(projectile)

func attack(target: Target) -> AttackApply:
	if projectile_delay == 0:
		_create_and_fire(target)
	else:
		attack_process.append({"time": Time.get_ticks_msec() + projectile_delay * 1000, "target": target})
	# Return a zero attack apply
	return _attack_application
