class_name Objective extends Node

@export var targets: Array[Target]

var targets_left: int

signal objective_complete

func _on_target_destroyed():
	targets_left -= 1
	if targets_left == 0:
		print("Objective complete!")
		objective_complete.emit()

func _ready():
	targets_left = len(targets)
	for t in targets:
		t.health.on_zero.connect(_on_target_destroyed)
