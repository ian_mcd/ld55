class_name AttackEffect extends Node

@export
var summon_effect: PackedScene

@export
var target_effect: PackedScene

@onready
var summon: Summon = $"../../.."

func apply(target: Target):
	var root = StageManager.instance.current_stage
	if summon_effect != null:
		var effect = create_effect(summon_effect)
		root.add_child(effect)
		effect.global_position = summon.global_position
		effect.global_basis = summon.global_basis
	if target_effect != null:
		var effect = create_effect(target_effect)
		root.add_child(effect)
		effect.global_position = target.global_position
		effect.global_basis = target.global_basis
		
func create_effect(effect: PackedScene) -> Node3D:
	var node: Node3D = effect.instantiate() as Node3D
	return node
