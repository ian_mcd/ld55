class_name Health extends Node

const DEFAULT_MAX_HEALTH: int = 30

@export
var max_health: int = DEFAULT_MAX_HEALTH

var health: int
signal on_zero

func _ready():
	health = max_health

func damage(damage_amount: int):
	health -= damage_amount
	health = max(health, 0)
	if health == 0:
		on_zero.emit()

func heal(heal_amount: int):
	var oh = heal_amount
	heal_amount = clamp(heal_amount, 0, max_health-health)
	print("{0} was healed {1} atempted {2}".format([get_parent().get_parent(), heal_amount, oh]))
	health += heal_amount
