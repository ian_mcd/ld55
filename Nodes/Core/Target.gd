class_name Target extends Node3D


enum Allegiance {NEUTRAL, PLAYER, ENEMY}

@export
var _allegiance: Allegiance

var allegiance: Allegiance:
	get: return _allegiance
	set(value):
		on_allegiance_change.emit(self, _allegiance, value)
		Target.update_allegiance(self, _allegiance, value)
		_allegiance = value

var health: Health

signal on_allegiance_change(target: Target, from: Allegiance, to: Allegiance)

static var targets: Dictionary = {
	Allegiance.NEUTRAL: {},
	Allegiance.PLAYER: {},
	Allegiance.ENEMY: {}
}

static func update_allegiance(target: Target, from: Allegiance, to: Allegiance):
	targets[from].erase(target)
	targets[to][target] = null


static func get_all_of_allegiance(search_allegiance: Allegiance) -> Array[Target]:
	var array: Array[Target] = []
	for t in targets[search_allegiance]:
		array.append(t)
	return array

static func get_closest_allegiance(target: Target, search_allegiance: Allegiance) -> Target:
	var dist: float = INF
	var closest: Target = null
	for t: Target in targets[search_allegiance]:
		if t == target:
			continue
		var curr_dist: float = t.global_position.distance_to(target.global_position)
		if curr_dist < dist:
			dist = curr_dist
			closest = t
	return closest
	
static func get_closest_neutral(target: Target) -> Target:
	return get_closest_allegiance(target, Allegiance.NEUTRAL)

static func get_closest_player(target: Target) -> Target:
	return get_closest_allegiance(target, Allegiance.PLAYER)

static func get_closest_enemy(target: Target) -> Target:
	return get_closest_allegiance(target, Allegiance.ENEMY)

static func get_oposite_allegiance(search_allegiance: Allegiance) -> Allegiance:
	var enemy_allegiance: Allegiance = Allegiance.NEUTRAL
	if search_allegiance == Allegiance.PLAYER:
		enemy_allegiance = Allegiance.ENEMY
	elif search_allegiance == Allegiance.ENEMY:
		enemy_allegiance = Allegiance.PLAYER
	return enemy_allegiance

static func _safe_invert_zero(val: float):
	if val == 0:
		return 0
	else:
		return 1 / val

static func get_weighted_oponent_dir(target: Target) -> Vector3:
	var sum: Vector3 = Vector3.ZERO
	for t in targets[get_oposite_allegiance(target.allegiance)]:
		var dir: Vector3 = t.global_position - target.global_position
		sum += Vector3(_safe_invert_zero(dir.x), 0, _safe_invert_zero(dir.z))

	return sum.normalized()


static func get_closest_oponent(target: Target) -> Target:
	var enemy_allegiance: Allegiance =get_oposite_allegiance(target.allegiance)
	return get_closest_allegiance(target, enemy_allegiance)


func update_model():
	for child in get_children():
		if child is Health:
			health = child
			return

func apply(attack_apply: AttackApply):
	if attack_apply.damage > 0:
		health.damage(attack_apply.damage)
	if attack_apply.heal > 0:
		health.heal(attack_apply.heal)
	print("{0} now at {1}hp".format([get_parent(), health.health]))


func _ready():
	targets[allegiance][self] = null

func _exit_tree():
	targets[allegiance].erase(self)

