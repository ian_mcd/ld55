extends Node3D
class_name Spin
var rng = RandomNumberGenerator.new()
@export var rotationSpeed : int
var currentRot_y : int
# Called when the node enters the scene tree for the first time.
func _ready():
	rotationSpeed = rng.randf_range(1,2)
	rotate_y(rng.randf_range(0,359))
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	rotate_y(currentRot_y + rotationSpeed * delta)
	pass

