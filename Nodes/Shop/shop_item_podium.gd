class_name ShopItemPodium extends Node3D

var summon_data: SummonData
@export
var spin: Spin

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func set_data(sd: SummonData):
	summon_data = sd
	spin.add_child(summon_data.display.instantiate())
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	pass
