class_name ShopPodium extends Node3D
var rng = RandomNumberGenerator.new()

@export var itemPool = ["Snorch", "Snobar", "Levtum"]
@export var shop_item_podium: ShopItemPodium

@onready
var _pod_item: ShopItemPodium = $".."

var itemCost = randi() % 30
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	pass

func getItemAttatched() -> String:
	return _pod_item.summon_data.name
	
func getItemCost() -> int:
	return itemCost
	
