extends Node3D
class_name ShopManager

var rng = RandomNumberGenerator.new()
@export var shopPool: ShopPool
@export var shopScene: PackedScene

var totalRarity : float
var shop_instance: Node3D

static var instance: ShopManager

signal shopping_complete

@onready
var timer: Timer = $Timer

func _ready():
	instance = self

func create_shop():
	shop_instance = shopScene.instantiate()
	add_child(shop_instance)
	
	for i in shopPool.summons:
		totalRarity += i.rarity
	PopulateMonsters()
	timer.timeout.connect(end_shop)
	timer.start()
	
func end_shop():
	timer.stop()
	unload_shop()
	shopping_complete.emit()

func unload_shop():
	remove_child(shop_instance)
	if shop_instance != null:
		shop_instance.queue_free()
		shop_instance = null
	
func selectShopPool ():
	var randomNum = rng.randf_range(0, totalRarity)
	var runningSum = 0
	for i in shopPool.summons:
		runningSum += i.rarity
		if runningSum > randomNum:
			return i
	return shopPool.summons[-1]

func PopulateMonsters() -> void:
	var my_group_members = get_tree().get_nodes_in_group("ShopPodium")
	for i: ShopItemPodium in my_group_members:
		i.set_data(selectShopPool())
		print("added monster")
