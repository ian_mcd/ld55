
class_name RoundRobinAttackPattern extends AttackPattern

var attacks: Array[Attack]

var _idx: int = 0

var _prev_attack_time: float

func get_current_attack() -> Attack:
	return attacks[_idx]

var prev_attack: Attack:
	get:
		var prev = (len(attacks) + _idx - 1) % len(attacks)
		return attacks[prev]

func update_model():
	attacks = []
	for child in get_children():
		if child is Attack:
			attacks.append(child as Attack)


func _ready():
	# Start -attack cooldown seconds back because we can attack right away
	update_model()
	_prev_attack_time = Time.get_ticks_msec() - prev_attack.cooldown

func pass_attack():
	_idx = (_idx + 1) % len(attacks)


func do_attack(target: Target) -> AttackApply:
	# TODO: self managed time
	_prev_attack_time = Time.get_ticks_msec()
	var attack_apply: AttackApply = current_attack.do_attack(target)
	on_attack.emit(current_attack)
	_idx = (_idx + 1) % len(attacks)
	return attack_apply

# returns true when a cooldown is compelete
func is_cooldown() -> bool:
	return Time.get_ticks_msec() - _prev_attack_time >= prev_attack.cooldown

func in_range(summon: Summon, target: Target) -> bool:
	return summon.target.global_position.distance_to(target.global_position) < current_attack.attack_range

func target_allegiance() -> Target.Allegiance:
	return current_attack.alligence
