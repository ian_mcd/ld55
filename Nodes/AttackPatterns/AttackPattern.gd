class_name AttackPattern extends ModelNode

signal on_attack(attack: Attack)

var current_attack: Attack:
	get: return get_current_attack()

func do_attack(_target: Target) -> AttackApply:
	return null

func get_current_attack() -> Attack:
	return null

# returns true when a cooldown is compelete
func is_cooldown() -> bool:
	return false

# Takes the summon applying the attack and the target of the attack.
func in_range(summon: Summon, target: Target) -> bool:
	return false

func target_allegiance() -> Target.Allegiance:
	return Target.Allegiance.NEUTRAL

func pass_attack():
	pass
