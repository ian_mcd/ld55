class_name SingleAttackPattern extends AttackPattern

var attack: Attack

var _prev_attack_time: float

func update_model():
	for child in get_children():
		if child is Attack:
			attack = child as Attack
			return

func _ready():
	# Start -attack cooldown seconds back because we can attack right away
	update_model()
	_prev_attack_time = Time.get_ticks_msec() - attack.cooldown

func do_attack(target: Target) -> AttackApply:
	# TODO: self managed time
	_prev_attack_time = Time.get_ticks_msec()
	on_attack.emit(attack)
	return attack.do_attack(target)

# returns true when a cooldown is compelete
func is_cooldown() -> bool:
	return Time.get_ticks_msec() - _prev_attack_time >= attack.cooldown

func in_range(summon: Summon, target: Target) -> bool:
	return summon.target.global_position.distance_to(target.global_position) < attack.attack_range

func target_allegiance() -> Target.Allegiance:
	return attack.alligence

func get_current_attack() -> Attack:
	return attack
