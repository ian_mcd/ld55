class_name Projectile extends Node3D


@export
var projectile_speed: float

var _target: Target

var _attack_apply: AttackApply

var _last_location

var _spawn_time: float
@export var timeout_sec: float = 10

signal on_hit(target: Target)

func create(target: Target, attack_apply: AttackApply):
	_attack_apply = attack_apply
	_target = target

func _ready():
	_spawn_time = Time.get_ticks_msec() 

func _process(delta):
	if not _target:
		_terminate()
		return
	move(delta)
	if Time.get_ticks_msec() > _spawn_time + timeout_sec * 1000:
		_terminate()

func move(delta):
	if _target != null:
		_last_location = _target.global_position
	
	var dir: Vector3 = global_position.direction_to(_last_location)
	var mag = min(projectile_speed * delta, global_position.distance_to(_last_location))
	global_position += mag * dir
	
	# Hit condition
	if mag < projectile_speed * delta:
		if _target != null:
			on_hit.emit(_target)
			_target.apply(_attack_apply)
		_terminate()

func _impact():
	_terminate()


func _terminate():
	print("terminate")
	get_parent().remove_child(self)
	queue_free()
