class_name ClosestAllyTargetingPattern extends TargetingPattern

func get_target(summon: Summon) -> Target:
	# TODO: Make better.
	return Target.get_closest_allegiance(summon.target, summon.target.allegiance)
