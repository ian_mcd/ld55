class_name LowestHpAllyTargetingPattern extends TargetingPattern

func get_target(summon: Summon) -> Target:
	# TODO: Make better.
	var targ: Target = null
	var lowest_hp: float = 1
	
	for t: Target in Target.get_all_of_allegiance(summon.target.allegiance):
		if t == summon.target:
			continue
		var hp_ratio = 1.0 * t.health.health / t.health.max_health
		if hp_ratio < lowest_hp and t.health.max_health != t.health.health:
			lowest_hp = hp_ratio
			targ = t
	
	return targ
