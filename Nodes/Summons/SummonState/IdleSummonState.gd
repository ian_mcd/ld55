class_name IdleSummonState extends SummonState

func enter():
	pass

func update(_delta: float):
	pass

func physical_update(_delta: float):
	var target: Target = summon.attack_pattern.current_attack.targeting_pattern.get_target(summon)
	if target != null:
		transitioned.emit(self, SummonStateKey.MOVE_TO)
	else:
		summon.attack_pattern.pass_attack()
		

func exit():
	pass
