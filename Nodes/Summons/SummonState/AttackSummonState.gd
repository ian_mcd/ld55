class_name AttackSummonState extends SummonState

var target: Target

func enter():
	# Choose target.
	target = summon.attack_pattern.current_attack.targeting_pattern.get_target(summon)
	#print("{0} ATTACKING {1}".format([summon, target.get_parent()]))

func update(_delta: float):
	pass

func physical_update(delta: float):
	if target == null:
		transitioned.emit(self, SummonStateKey.IDLE)
		return

	summon.face(target.global_position, delta)

	if not summon.attack_pattern.in_range(summon, target):
		transitioned.emit(self, SummonStateKey.MOVE_TO)
		return

	if summon.attack_pattern.is_cooldown():
		summon.attack_pattern.do_attack(target)
		target = summon.attack_pattern.current_attack.targeting_pattern.get_target(summon)

func exit():
	pass
