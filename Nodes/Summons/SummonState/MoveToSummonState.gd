class_name MoveToSummonState extends SummonState

var target: Target
var navigation_agent: NavigationAgent3D
func enter():
	# Choose target.
	target = summon.attack_pattern.current_attack.targeting_pattern.get_target(summon)
	navigation_agent = summon.navigation_agent
	summon.navigation_agent.avoidance_enabled = true
	if not target:
		transitioned.emit(self, SummonStateKey.IDLE)
		return
	print("{0} targeting {1}".format([summon, target.get_parent()]))
	

func update(_delta: float):
	pass

func physical_update(delta: float):
	#var n_dir: Vector3 = (summon.global_position - target.global_position).normalized()
	if target == null:
		transitioned.emit(self, SummonStateKey.IDLE)
		return

	summon.set_movement_target(target.global_position)

	if not navigation_agent.is_navigation_finished():
		var next_path_position: Vector3 = navigation_agent.get_next_path_position()
		var new_velocity: Vector3 = summon.global_position.direction_to(next_path_position) * summon.movement_speed
		if navigation_agent.avoidance_enabled:
			navigation_agent.set_velocity(new_velocity)
		else:
			summon.move(new_velocity, delta)

	if summon.attack_pattern.in_range(summon, target):
		transitioned.emit(self, SummonStateKey.ATTACK)

func exit():
	summon.set_movement_target(summon.global_position)
	summon.navigation_agent.avoidance_enabled = false
