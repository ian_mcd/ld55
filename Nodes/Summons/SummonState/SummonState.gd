class_name SummonState extends RefCounted

signal transitioned(prev_state: SummonState, next_state: SummonStateKey)

enum SummonStateKey {IDLE, MOVE_TO, ATTACK}

var summon: Summon

func enter():
	pass

func update(_delta: float):
	pass

func physical_update(_delta: float):
	pass

func exit():
	pass
