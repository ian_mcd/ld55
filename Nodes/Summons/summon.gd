class_name Summon extends CharacterBody3D

"""
Summons are summonable peeps in to be had.  For them to work they need specific children.

1. a child in the "SummonModel" group that contains the model of the summon
2. an attack pattern
3. a Target (making it targatable)
4. a navigation agent to navigate
"""

#region consts
const DEFAULT_ROT_SPEED = 1
const SCALE_FACTOR = .25
#endregion

#region configs
@export var rot_speed: float = DEFAULT_ROT_SPEED
@export var movement_speed: float = 10
@export var powerLevel: int = 1
@export var on_death_effect: PackedScene
#endregion

#region model
var attack_pattern: AttackPattern
var summon_state: SummonState
var target: Target
var navigation_agent: NavigationAgent3D
var model: Node3D
#endregion

#region statemachine_state
var _state_machine_states: Dictionary = {}
#endregion

#region signals
signal on_death()
#endregion

static var summons = {}

#region statemachine
signal on_transition(prev_state: SummonState, new_state: SummonState.SummonStateKey)


func _handle_transition(state: SummonState, to: SummonState.SummonStateKey):
	state.exit()
	summon_state = _state_machine_states[to]
	print("{0} moving to {1}".format([self, SummonState.SummonStateKey.find_key(to)]))
	on_transition.emit(state, to)
	summon_state.enter()


func _init_state_machine():
	# Initialize the states
	_state_machine_states[SummonState.SummonStateKey.IDLE] = IdleSummonState.new()
	_state_machine_states[SummonState.SummonStateKey.IDLE].summon = self
	_state_machine_states[SummonState.SummonStateKey.IDLE].transitioned.connect(_handle_transition)

	_state_machine_states[SummonState.SummonStateKey.ATTACK] = AttackSummonState.new()
	_state_machine_states[SummonState.SummonStateKey.ATTACK].summon = self
	_state_machine_states[SummonState.SummonStateKey.ATTACK].transitioned.connect(_handle_transition)

	_state_machine_states[SummonState.SummonStateKey.MOVE_TO] = MoveToSummonState.new()
	_state_machine_states[SummonState.SummonStateKey.MOVE_TO].summon = self
	_state_machine_states[SummonState.SummonStateKey.MOVE_TO].transitioned.connect(_handle_transition)

	print("{0} init state machine".format([self]))	
	summon_state = _state_machine_states[SummonState.SummonStateKey.IDLE]

func _update_state_machine(delta):
	summon_state.update(delta)
	
func _physics_update_state_machine(delta):
	summon_state.physical_update(delta)
#endregion

#region model
func update_model():
	var num_items: int = 4
	for child in get_children():
		if child is Target: 
			target = child
			target.update_model()
			num_items -= 1
		elif child is NavigationAgent3D:
			navigation_agent = child
			num_items -= 1
		elif child is AttackPattern:
			attack_pattern = child
			attack_pattern.update_model()
			num_items -= 1
		elif child.is_in_group("SummonModel"):
			model = child
			num_items -= 1
		
		# Early return.
		if num_items == 0:
			return

		# TODO: probably log an error here if num_items != 0
#endregion

#region godot_calls
# Not a godot call just relevant to the init.
func _get_configuration_warnings():
	var warnings = []
	if target == null:
		warnings.append("Summon must have a child of type Target")
	if attack_pattern == null:
		warnings.append("Summon must have a child of type AttackPattern")
	if navigation_agent == null:
		warnings.append("Summon must have a child of type NavigationAgent3D")
		
	return warnings

func _init():
	update_configuration_warnings()

func _enter_tree():
	# Call update model to set values
	update_model()

func _ready():
	summons[self] = null  #yolo
	update_level(powerLevel)

	if on_death_effect == null:
		on_death_effect = load("res://Art/VFX/deathVFX.tscn")

	target.health.on_zero.connect(_destroy_on_zero_hp)
	
	navigation_agent.max_speed = movement_speed
	navigation_agent.avoidance_layers = target.allegiance
	
	# initialize the state machine.
	_init_state_machine()

	# Connect the nav agent
	navigation_agent.velocity_computed.connect(Callable(move_callback))
	print("{0} ready".format([self]))


func _process(delta):
	_update_state_machine(delta)

func _exit_tree():
	summons.erase(self)

func _physics_process(delta):
	_physics_update_state_machine(delta)
#endregion


func _to_string() -> String:
	return "{0}:{1}:{2}".format([
		Target.Allegiance.find_key(target.allegiance),
		get_class(),
		name
	])
	
func update_level(level: int):
	powerLevel = level
	var scaleFactor = 1 + (powerLevel * SCALE_FACTOR)
	self.model.scale = Vector3(scaleFactor, scaleFactor, scaleFactor)

func _destroy_on_zero_hp():
	on_death.emit()
	if on_death_effect != null:
		var effect: Node3D = on_death_effect.instantiate() as Node3D
		StageManager.instance.current_stage.add_child(effect)
		effect.global_position = global_position
		effect.global_basis = global_basis
		
	queue_free()

func initialize(allegiance: Target.Allegiance):
	update_model()
	target.allegiance = allegiance

#region nav
func set_movement_target(movement_target: Vector3):
	navigation_agent.set_target_position(movement_target)

func face(look_to_position: Vector3, delta):
	var look: Vector3 = look_to_position - model.global_position
	# Flatten
	#look = Vector3(look.x, 0, look.y)
	look = look.slide(Vector3.UP)

	look = look.normalized()
	look = look.rotated(Vector3.UP, PI)
	# Angle between forward and look
	var angle: float = (-model.get_global_transform().basis.z).signed_angle_to(look, Vector3.UP)
	var max_val = delta * rot_speed * 2*PI
	angle = clamp(angle, -max_val, max_val)
	model.rotate_y(angle)

func move_callback(safe_velocity: Vector3):
	var delta = get_physics_process_delta_time()
	return move(safe_velocity, delta)

func move(safe_velocity: Vector3, delta):
	velocity = safe_velocity
	if model != null and velocity.length() != 0:
		face(model.global_position + velocity, delta)
	move_and_slide()
#endregion
