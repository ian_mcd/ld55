extends Resource

class_name SummonData

@export var name: String
@export var desc: String
@export var powerLevel: int # used for stats and size of character
@export var hasSnoblar: bool
@export var hasSnorch: bool
@export var hasTrocto: bool
@export var hasLevturn:bool
@export var rarity:float

# The amount of points it costs to include this as an enemy
@export var point_cost: int

@export var thumbnail: Texture2D
@export var scene: PackedScene
@export var display: PackedScene
@export var cooldown: int	

@export var level: int
