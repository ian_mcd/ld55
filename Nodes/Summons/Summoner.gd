class_name Summoner extends Node3D

# Stay away from the summoner! https://www.youtube.com/watch?v=0eceLxLWoDU

@export
var deck: Deck 

static var RAY_LENGTH = 1000
var summon_radius = 5

var hand_size = 5
var cards_in_hand = 0

var selectedCard: Card = null

static var instance: Summoner

@export var summoning_circle: Node3D

# Called when the node enters the scene tree for the first time.
func _ready():
	instance = self
	StageManager.instance.on_stage_load.connect(on_stage_load)
	
	if summoning_circle != null:
		summoning_circle.visible = false;
		summoning_circle.scale = Vector3(summon_radius, 1, summon_radius);
	#while deck.cards_remaining() > 0:
		#cards_in_hand = cards_in_hand + 1
		#deck.draw_card()
	
func on_stage_load(new_stage_data: StageData, stage: Stage):
	print("reloading deck")
	deck.load_deck()

func get_click_event(event):
	print("getting summon location!")
	var space_state = get_world_3d().direct_space_state
	var camera3d = get_viewport().get_camera_3d()
	# 	var mousepos = get_viewport().get_mouse_position()
	var from = camera3d.project_ray_origin(event.position)
	var to = from + camera3d.project_ray_normal(event.position) * RAY_LENGTH
	var query = PhysicsRayQueryParameters3D.create(from, to, 2)
	query.collide_with_areas = true
	var result = space_state.intersect_ray(query)
	if result.size() > 0:
		return result
	return null

func _process(_delta):
	if summoning_circle != null:
		summoning_circle.visible = selectedCard != null;

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(_delta):
	pass

func _input(event):
	if GameManager.instance.is_shoping:
		return
	if event.is_action_pressed("summon"):
		print("you tried to summon!")
		var clicked_item = get_click_event(event)
		if clicked_item == null:
			print("didn't click anything")
		elif clicked_item.collider.get_parent() is Card:
			var clicked_card = clicked_item.collider.get_parent() as Card
			if selectedCard != null:
				deck.unselect_card(selectedCard)
			if selectedCard != clicked_card:
				selectedCard = clicked_card
				deck.select_card(clicked_card)
			else:	
				selectedCard = null
			print("You selected a card!")
		elif selectedCard != null:
			var pos = clicked_item.position
			
			if pos != null && pos.distance_to(self.global_position) < summon_radius:
				print("got summon position! now creating little guy!")
				
				var summon_boi = selectedCard.summon_data.scene.instantiate() as Summon
				if summon_boi == null:
					print("SUMMON: Summon fail null for data {0}".format(selectedCard.summon_data.scene))
				summon_boi.set_position(pos)
				summon_boi.initialize(Target.Allegiance.PLAYER)
				summon_boi.update_level(selectedCard.summon_data.powerLevel)
				StageManager.instance.current_stage.add_child(summon_boi)
				selectedCard.visible = false	
				deck.unselect_card(selectedCard)
				deck.discard_card(selectedCard)
				selectedCard = null
				deck.refresh_hand()
			else:
				print("failed to get valid position for summon!")
		else:
			print("didn't select a card... :(")
			
	if event.is_action_pressed("toggle_cards"):
		if deck.deckVisible:
			selectedCard = null
		deck.toggle_view_deck()
