class_name SummonAnimationManager extends Node

@export var idle_name: String
@export var move_name: String

var summon: Summon

var player: AnimationPlayer

func _enter_tree():
	summon  = $".."
	
	if player == null:
		for c in summon.model.get_children():
			if c is AnimationPlayer:
				player = c

func _ready():
	summon.on_transition.connect(_on_transition)
	summon.attack_pattern.on_attack.connect(_on_attack)
	

func _process(_delta):
	pass

func _on_attack(attack: Attack):
	player.play(attack.animation_name)
	player.animation_set_next(attack.animation_name, idle_name)

func _on_transition(_prev: SummonState, state: SummonState.SummonStateKey):
	trigger_animation(state)

func trigger_animation(state: SummonState.SummonStateKey):
	if player == null:
		print("AnimationPlayer not found")
		return
	
	var anim_name: String = idle_name
	if state == SummonState.SummonStateKey.MOVE_TO:
		anim_name = move_name
	
	player.play(anim_name)

