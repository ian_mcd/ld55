class_name Combiner extends Node3D

# Stay away from the summoner! https://www.youtube.com/watch?v=0eceLxLWoDU

@export
var deck: Deck

static var RAY_LENGTH = 1000
var summon_radius = 5

var hand_size = 5
var cards_in_hand = 0

var selectedCards: Array[Card] = []

func index_of(cardWanted: Card):
	for i in range(selectedCards.size()):
		if selectedCards[i] == cardWanted:
			return i
	return -1

func get_click_event(event):
	var space_state = get_world_3d().direct_space_state
	var camera3d = get_viewport().get_camera_3d()
	# 	var mousepos = get_viewport().get_mouse_position()
	var from = camera3d.project_ray_origin(event.position)
	var to = from + camera3d.project_ray_normal(event.position) * RAY_LENGTH
	var query = PhysicsRayQueryParameters3D.create(from, to, 2)
	query.collide_with_areas = true
	var result = space_state.intersect_ray(query)
	if result.size() > 0:
		return result
	return null
	
func _input(event):
	if event.is_action_pressed("combine_picker"):
		var clicked_item = get_click_event(event)
		if clicked_item == null:
			print("didn't click anything")
		elif clicked_item.collider.get_parent() is Card:
			var clicked_card = clicked_item.collider.get_parent() as Card
			var index = index_of(clicked_card)
			if index != -1:
				selectedCards.pop_at(index)
				deck.unselect_card(clicked_card)
				print("deselect")
			elif selectedCards.size() < 2:
				deck.select_card(clicked_card)
				selectedCards.append(clicked_card)
				print("select")
	
	if event.is_action_pressed("combine"):
		if selectedCards.size() == 2:
			deck.combine_card(selectedCards[0], selectedCards[1])
			deck.unselect_card(selectedCards[0])
			selectedCards[0].visible = false
			deck.unselect_card(selectedCards[1])
			selectedCards[1].visible = false
			selectedCards = []
			print("combined!")
