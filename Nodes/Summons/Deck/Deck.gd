class_name Deck extends Node

var cards: Array[Card]
var discards: Array[Card]

var deckVisible: bool = false

# Called when the node enters the scene tree for the first time.
func _ready():
	load_deck()

func load_deck():
	cards = []
	print("DECK: loading cards into deck")
	print("DECK:" + str(get_children()))
	print("DECK:" + str(get_children(true)))
	for kiddo in self.get_children():
		print("DECK: adding card {0}".format([kiddo]))
		cards.append(kiddo)
	discards = []
	refresh_hand()

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	pass
	
func cards_remaining():
	return cards.size()
	
func draw_card():
	var card = cards.pop_front()
	discards.append(card)
	return card
	
func index_of(cardWanted: Card):
	for i in range(cards.size()):
		if cards[i] == cardWanted:
			return i
	return -1

func add_card(card: Card):
	card.visible = deckVisible
	cards.append(card)
	add_child(card)
	refresh_hand()

func discard_card(card: Card):
	cards.pop_at(index_of(card))
	discards.append(card)
	card.position = Vector3(100, 100, 100)
	
func combine_card(card1: Card, card2: Card):
	var curCardIndex1 = index_of(card1)
	var curCardIndex2 = index_of(card2)
	if (curCardIndex1 != -1 && curCardIndex2 != -1):
		cards.pop_at(curCardIndex1)
		cards.pop_at(index_of(card2))
		remove_child(card1)
		remove_child(card2)
		card1.queue_free()
		card2.queue_free()
		var newCard = card1.combine_card(card2)
		cards.append(newCard)
		self.add_child(newCard)
		newCard.visible = deckVisible
		refresh_hand()

func refresh_hand():
	var xOffset = 1.3
	var cam: Camera3D = get_viewport().get_camera_3d()
	print("HANDING: " + str(cam.get_global_transform().basis))
	print("HANDING: " + str(cam.global_position))
	for i in range(cards.size()):
		var card = cards[i]
		card.visible = deckVisible
		if cam != null:
			print("HANDING HAND")
			#card.position = Vector3(-8.5 + xOffset * i, 10, 23)
			card.global_position = cam.global_position + (
				Vector3(-8 + xOffset * i, 0, 0)
				- cam.get_global_transform().basis.z.normalized() * 7
				- cam.get_global_transform().basis.y * 3.5
			)
			print("HANDING: "+ str(card.global_position))
			card.rotation = Vector3(-45, 0, 0)

# Show the whole deck as a list of cards (thumbnails) on screen. We can use this to click cards we want to combine		
func toggle_view_deck():
	for i in range(cards.size()):
		var card = cards[i]
		card.visible = not deckVisible
	deckVisible = !deckVisible
	refresh_hand()

func select_card(card: Card):
	card.position = card.position + Vector3(0, .5, 0)

func unselect_card(card: Card):
	card.position = card.position - Vector3(0, .5, 0)
	
	
