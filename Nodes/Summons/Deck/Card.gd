class_name Card extends Node3D

#@export
@export var summon_data: SummonData
# var summon = load("res://Nodes/Summons/simple_summon.tscn")

#TOD0: add display to all the summonDatas, commment back in line 8 and 13
var base_sprite = load("res://Art/UI/card.png")

# Called when the node enters the scene tree for the first time.
func _ready():
	var sprite = Sprite3D.new()
	sprite.texture = summon_data.thumbnail
	sprite.scale = Vector3(.1, .1, .1)
	sprite.render_priority = 2
	self.add_child(sprite)
	var base = Sprite3D.new()
	base.texture = base_sprite
	base.scale = Vector3(.1, .1, .1)
	base.render_priority = 1
	
	self.add_child(base)
	self.visible = false


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	pass

func combine_card(newCard: Card):
	var newLevel = min(10, newCard.summon_data.powerLevel + summon_data.powerLevel)
	var newHasSnoblar = newCard.summon_data.hasSnoblar || summon_data.hasSnoblar
	var newHasSnorch = newCard.summon_data.hasSnorch || summon_data.hasSnorch
	var newHasTrocto = newCard.summon_data.hasTrocto || summon_data.hasTrocto
	var newHasLevturn = newCard.summon_data.hasLevturn || summon_data.hasLevturn
	var newSummonData = Card.summonDataMapping(newLevel, newHasSnoblar, newHasSnorch, newHasTrocto, newHasLevturn)
	var combinedCard = Player.player.make_card(newSummonData)
	return combinedCard

static func summonDataMapping(level: int,
							  hasSnoblar: bool,
							  hasSnorch: bool,
							  hasTrocto: bool,
							  hasLevturn: bool):
	if (hasSnoblar && hasSnorch && hasTrocto && hasLevturn && level >= 10):
		return load(append_level("God", 10)) # TODO replace with god tier boi
	if (hasSnoblar && hasSnorch && hasTrocto && hasLevturn):
		return load(append_level("Chimera/Levtroblarch/Levtroblarch", level))
	elif (hasSnoblar && hasSnorch && hasTrocto):
		return load(append_level("Chimera/Troblarch/Troblarch", level))
	elif (hasSnoblar && hasTrocto && hasLevturn):
		return load(append_level("Chimera/Levtroblar/Levtroblar", level))
	elif (hasSnoblar && hasSnorch && hasLevturn):
		return load(append_level("Chimera/Levblarch/Levblarch", level))
	elif (hasSnorch && hasTrocto && hasLevturn):
		return load(append_level("Chimera/Levtroch/Levtroch", level))
	elif (hasSnoblar && hasSnorch):
		return load(append_level("Chimera/Blarch/Blarch", level))
	elif (hasSnoblar && hasTrocto):
		return load(append_level("Chimera/Troblar/Troblar", level))
	elif (hasSnoblar && hasLevturn):
		return load(append_level("Chimera/Levblar/Levblar", level))
	elif (hasSnorch && hasTrocto):
		return load(append_level("Chimera/Troch/Troch", level))
	elif (hasSnorch && hasLevturn):
		return load(append_level("Chimera/Levch/Levch", level))
	elif (hasTrocto && hasLevturn):
		return load(append_level("Chimera/Levtro/Levtro", level))
	elif (hasSnoblar):
		return load(append_level("Snoblar/Snoblar", level))
	elif (hasSnorch):
		return load(append_level("Snorch/Snorch", level))
	elif (hasTrocto):
		return load(append_level("Trocto/Trocto", level))
	else:
		return load(append_level("Levturn/Levturn", level))						
	
static func append_level(summon: String, level: int):
	print("res://Nodes/Summons/SummonData/" + summon + str(level) + ".tres")
	return "res://Nodes/Summons/SummonData/" + summon + str(level) + ".tres"
