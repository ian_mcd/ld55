class_name SummonEnemySummonerState extends EnemySummonerState

const DEFAULT_WARMUP: float = 1

var warmup: float = DEFAULT_WARMUP

var _start_warmup_time: float

func enter():
	print("EnemySummon")
	_start_warmup_time = Time.get_ticks_msec()


func physical_update(_delta: float):
	if _start_warmup_time + warmup > Time.get_ticks_msec():
		return

	summoner.summon()
	if summoner.enemy_close():
		transitioned.emit(self, EnemySummonerStateKey.MOVE_TO)
	else:
		transitioned.emit(self, EnemySummonerStateKey.IDLE)

func exit():
	pass
