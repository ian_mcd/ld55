class_name MoveToEnemySummonerState extends EnemySummonerState

const MOVE_AMOUNT: float = 3

var _prev_pos: Vector3

func enter():
	_prev_pos = summoner.global_position
	var dir: Vector3 = - Target.get_weighted_oponent_dir(summoner.target)
	summoner.set_movement_target(summoner.global_position + dir * MOVE_AMOUNT)

func update(_delta: float):
	pass

func physical_update(_delta: float):
	# If im done or i cant move, summon.
	summoner.move()
	if summoner.cooled_down() and summoner.can_summon():
		transitioned.emit(self, EnemySummonerStateKey.SUMMON)
	
	if summoner.navigation_agent.is_navigation_finished() or _prev_pos.distance_to(summoner.global_position) < 0.00000000001:
		transitioned.emit(self, EnemySummonerStateKey.IDLE)
	_prev_pos = summoner.global_position
	

func exit():
	pass
