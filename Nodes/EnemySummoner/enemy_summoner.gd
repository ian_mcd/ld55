class_name EnemySummoner extends CharacterBody3D

const SPEED = 5.0

@onready
var navigation_agent: NavigationAgent3D = $NavigationAgent3D

var _state_machine_states: Dictionary = {}

var summoner_state: EnemySummonerState

var curr_cooldown: int = 0

var prev_summon_time: int = 0

@export
var summon_range = 3


@export
var run_range = 6


@onready
var deck: Deck = $Deck

@export
var target: Target

var anim_player: AnimationPlayer

func move():
	if not navigation_agent.is_navigation_finished():
		var next_path_position: Vector3 = navigation_agent.get_next_path_position()
		var new_velocity: Vector3 = global_position.direction_to(next_path_position) * SPEED
		if navigation_agent.avoidance_enabled:
			navigation_agent.set_velocity(new_velocity)
		else:
			set_velo(new_velocity)
	if anim_player  != null:
		if velocity.length() != 0:
			anim_player.play("summoner_walk")
		else:
			anim_player.play("summoner_idle")
			

func set_velo(safe_velocity: Vector3):
	velocity = safe_velocity
	move_and_slide()

func can_summon() -> bool:
	return deck.cards_remaining() > 0

func summon():
	# Get summon position
	var closest_enemy: Target = Target.get_closest_oponent(target)
	var pos: Vector3 = Vector3.ZERO
	if closest_enemy != null:
		pos = global_position + global_position.direction_to(closest_enemy.global_position) * clamp(
			global_position.distance_to(closest_enemy.global_position) - 1,
			0,
			summon_range
		)
	else:
		pos = global_position + Vector3.BACK * 3
	
	print("spawning enemy from deck size {0}".format([deck.cards_remaining()]))
	var data: SummonData = deck.draw_card().summon_data
	var summon_boi = data.scene.instantiate() as Summon

	curr_cooldown = data.cooldown
	if summon_boi as Summon == null:
		print("{0} failed to summon".format([self]))
		return
	summon_boi.set_position(pos)
	summon_boi.initialize(target.allegiance)
	StageManager.instance.current_stage.add_child(summon_boi)

	# Set summon time.
	prev_summon_time = Time.get_ticks_msec()


func set_movement_target(movement_target: Vector3):
	navigation_agent.set_target_position(movement_target)


func _handle_transition(_state: EnemySummonerState, to: EnemySummonerState.EnemySummonerStateKey):
	summoner_state.exit()
	summoner_state = _state_machine_states[to]
	print("{0} moving to {1}".format([self, EnemySummonerState.EnemySummonerStateKey.find_key(to)]))
	summoner_state.enter()

func enemy_close() -> bool:
	var closest: Target = Target.get_closest_oponent(target)
	if closest == null:
		return false
	return closest.global_position.distance_to(global_position) < run_range

func cooled_down() -> bool:
	return Time.get_ticks_msec() > prev_summon_time + curr_cooldown * 1000

func _init_state_machine():
	# Initialize the states
	_state_machine_states[EnemySummonerState.EnemySummonerStateKey.IDLE] = IdleEnemySummonerState.new()
	_state_machine_states[EnemySummonerState.EnemySummonerStateKey.IDLE].summoner = self
	_state_machine_states[EnemySummonerState.EnemySummonerStateKey.IDLE].transitioned.connect(_handle_transition)
	
	_state_machine_states[EnemySummonerState.EnemySummonerStateKey.MOVE_TO] = MoveToEnemySummonerState.new()
	_state_machine_states[EnemySummonerState.EnemySummonerStateKey.MOVE_TO].summoner = self
	_state_machine_states[EnemySummonerState.EnemySummonerStateKey.MOVE_TO].transitioned.connect(_handle_transition)
	
	_state_machine_states[EnemySummonerState.EnemySummonerStateKey.SUMMON] = SummonEnemySummonerState.new()
	_state_machine_states[EnemySummonerState.EnemySummonerStateKey.SUMMON].summoner = self
	_state_machine_states[EnemySummonerState.EnemySummonerStateKey.SUMMON].transitioned.connect(_handle_transition)

	summoner_state = _state_machine_states[EnemySummonerState.EnemySummonerStateKey.IDLE]

func _update_state_machine(delta):
	summoner_state.update(delta)
	
func _physics_update_state_machine(delta):
	summoner_state.physical_update(delta)

func _ready():
	_init_state_machine()
	target.health.on_zero.connect(_on_kill)
	
	for c in $summoner.get_children():
		if c is AnimationPlayer:
			anim_player = c
	if anim_player != null:
		anim_player.play("summoner_idle")

func _on_kill():
	queue_free()

func _process(delta):
	_update_state_machine(delta)

func _physics_process(delta):
	_physics_update_state_machine(delta)
