class_name IdleEnemySummonerState extends EnemySummonerState

func enter():
		print("EnemyIdle")

func update(_delta: float):
	pass

func physical_update(_delta: float):
	if summoner.enemy_close():
		transitioned.emit(self, EnemySummonerStateKey.MOVE_TO)
		return
	if summoner.cooled_down() and summoner.can_summon():
		transitioned.emit(self, EnemySummonerStateKey.SUMMON)
		return

func exit():
	pass

