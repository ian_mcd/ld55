class_name EnemySummonerState extends RefCounted

signal transitioned(prev_state: EnemySummonerState, next_state: EnemySummonerState)

enum EnemySummonerStateKey {IDLE, MOVE_TO, SUMMON}

var summoner: EnemySummoner

func enter():
	pass

func update(_delta: float):
	pass

func physical_update(_delta: float):
	pass

func exit():
	pass

