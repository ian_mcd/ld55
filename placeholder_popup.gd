class_name ShopUI extends BoxContainer
# Called when the node enters the scene tree for the first time.

func _ready():
	self.hide()
	
	pass # Replace with function body.
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	pass
func _on_no_button_button_up():

	self.hide()
	pass # Replace with function body.
func _on_yes_button_button_up():

	self.hide()
	pass # Replace with function body.
func changePopupText(name_txt: String, cost: int):
	$Control/RichTextLabel.bbcode_text = "Would you like to buy {0} for ${1}?".format([name_txt, cost])
	pass
