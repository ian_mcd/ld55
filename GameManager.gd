class_name GameManager extends Node3D

@onready var player: Player = $Player
@onready var stage_manager: StageManager = $StageManager
@onready var shop_manager: ShopManager = $ShopManager
@export var loose_screen: PackedScene

static var instance: GameManager

var is_shoping: bool = false

# Called when the node enters the scene tree for the first time.
func _ready():
	print("SHOW OPENING MENU")
	instance = self

	wait_then_start_game()

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	pass

func wait_then_start_game():
	#await start_game_signal
	game_loop()

func on_loose():
	stage_manager.unload_current_stage()
	var loose = loose_screen.instantiate()
	add_child(loose)
	

func game_loop():	
	print("Starting Game Loop")
	Summoner.instance.deck.toggle_view_deck()
	while stage_manager._current_stage_idx < stage_manager.number_of_stages:
		# Load and Start next stage
		var stage = stage_manager.current_stage
		# wait for stage to be completed 
		print("Waiting for state objective")
		await stage.objective.objective_complete
		print("stage objective completed, moving on.")
		is_shoping = true
		
		# Unload stage and start shop
		stage_manager.unload_current_stage()
		shop_manager.create_shop()
		Summoner.instance.deck.load_deck()
		
		# Wait for player to complete shop
		await shop_manager.shopping_complete
		is_shoping = false
		#shop_manager.unload_shop()
		stage_manager.load_next_stage()
		
		# Await shop exit
	print("Game Loop Finished")
	# When no more stages are available loop exits
